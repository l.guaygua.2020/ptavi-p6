#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Simple SIP registrar
"""

import socketserver
import sys
import simplertp
import random


class SIPHandler(socketserver.BaseRequestHandler):

    def handle(self):
        metodos_validos = ("INVITE", "BYE", "ACK")
        data = self.request[0]
        sock = self.request[1]
        received = data.decode('utf-8')
        print(f"{self.client_address[0]} {self.client_address[1]} {received}")
        metodo = received.split()[0]
        if type(received) is not str:
            response = 'SIP/2.0 400 Bad Request'
            sock.sendto(response.encode('utf-8'), self.client_address)
        if metodo not in metodos_validos:
            response = 'SIP/2.0 405 Method Not Allowed'
            sock.sendto(response.encode('utf-8'), self.client_address)
        if metodo == metodos_validos[0]:
            trying = 'SIP/2.0 100 TRY'
            sock.sendto(trying.encode('utf-8'), self.client_address)
            ring = 'SIP/2.0 180 RING'  # REVISAR ESTO
            sock.sendto(ring.encode('utf-8'), self.client_address)
            cuerpo = "\n".join(received.split("\n")[1:])
            response = 'SIP/2.0 200 OK\n' + cuerpo
            sock.sendto(response.encode('utf-8'), self.client_address)
        if metodo == metodos_validos[1]:
            ok = 'SIP/2.0 200 OK'
            sock.sendto(ok.encode('utf-8'), self.client_address)
        if metodo == metodos_validos[2]:
            ALEAT = random.randint(0, 50000)
            ip = self.client_address[0]
            port = self.client_address[1]
            audio_file = sys.argv[3]

            RTP_header = simplertp.RtpHeader()
            RTP_header.set_header(pad_flag=0, ext_flag=0, cc=0, ssrc=ALEAT)
            audio = simplertp.RtpPayloadMp3(audio_file)
            simplertp.send_rtp_packet(RTP_header, audio, ip, port)


def main():
    usage = "Usage: python3 server.py <IP> <puerto> <fichero_audio>"
    if len(sys.argv) != 4:
        sys.exit(usage)
    try:
        ip, port, file = sys.argv[1:]
        port = int(port)
    except ValueError:
        sys.exit("el puerto debe ser 'int'")
    try:
        serv = socketserver.UDPServer((ip, port), SIPHandler)
        print(f"Server listening in port {port}")
    except OSError as e:
        sys.exit(f"Error while trying to listen: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Server done")
        sys.exit(0)


if __name__ == "__main__":
    main()
