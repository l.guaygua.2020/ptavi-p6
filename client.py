#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Simple client for a SIP registrar
"""

import socket
import sys

usage = 'Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>'


def conseguir_argv():
    metodos_validos = ("INVITE", "BYE", "ACK")
    try:
        if sys.argv[1] in metodos_validos:  # para especificar el tamaño exacto
            metodo = sys.argv[1]
        else:
            sys.exit(usage)
        msj = sys.argv[2]
        code = msj.split("@")
        name = code[0]
        argumentos = code[1].split(":")
        ip = argumentos[0]
        port = int(argumentos[1])
        # print(f"nombre: {name}, Ip: {ip}, Puerto: {port}, Metodo {metodo}")
    except:
        sys.exit(usage)

    return metodo, name, ip, port


def crearPeticion():
    metodo, name, ip, port = conseguir_argv()
    if metodo == "INVITE":
        msj = f"{metodo} sip:{name}@{ip} SIP/2.0\n"
        cuerpo = f"v=0\no={name}@gotham.com {ip}\ns=misesion\nt=0\nm=audio {port} RTP\n"
        tamanio = len(bytes(cuerpo, "utf-8"))
        cabecera = f"Content-Type: application/sdp\nContent-Length: {tamanio}\n\n"
        peticion = msj + cabecera + cuerpo
    else:
        peticion = f"{metodo} sip:{name}@{ip} SIP/2.0\n\n"
    return peticion


def main():
    fin = False
    metodo, name, ip, port = conseguir_argv()
    if metodo == 'INVITE':
        request = crearPeticion()
    elif metodo == 'BYE':
        request = crearPeticion()
    elif metodo == 'ACK':
        request = crearPeticion()
    else:
        sys.exit(usage)  # se termina si register no esta o masl escrito

    # Creamos el socket de envío, enviamos el mensaje de registro,
    # y esperamos respuesta.
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.sendto(request.encode('utf-8'), (ip, port))

            fin = False
            while not fin:
                data = my_socket.recv(1024)
                metodo_ack = data.decode("utf-8").split()[1]
                print(data.decode('utf-8'))
                if metodo_ack == '200':
                    request = f"ACK sip:{name}@{ip} SIP/2.0"
                    my_socket.sendto(request.encode('utf-8'), (ip, port))
                    fin = True
                else:
                    fin=False




    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
